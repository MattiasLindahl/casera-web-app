/************************************************
  Author: Elena Babenko
  last updated 2/7/2019
  lena.babenko@gmail.com
  Gulp 4
********************************************** */

"use strict";

/***************************
 Gulp Packages
************************** */

 // General
const browsersync = require("browser-sync").create();
const concat = require("gulp-concat");
const del = require("del");
const gulp = require("gulp");
const merge = require('merge-stream');
const path = require("path");
const plumber = require("gulp-plumber");
const rename = require("gulp-rename");

// CSS
const autoprefixer = require("autoprefixer");
const cssnano = require("cssnano");
const pixrem = require("gulp-pixrem");
const postcss = require("gulp-postcss");
const sass = require("gulp-sass");
const sourcemaps = require("gulp-sourcemaps");

// JS
//const eslint = require("gulp-eslint");
const uglify = require("gulp-uglify");

/***************************
 Paths to project folders
************************** */

const paths = {
  input: 'src/**/*',
  output: 'dist/',
  imgs: {
    input: 'src/assets/img/*',
    output: 'dist/assets/img/'
  },
  fonts: {
    input: 'src/assets/fonts/*',
    output: 'dist/assets/fonts/'
  },
  js: {
    input: 'src/js/*',
    output: 'dist/js',
    vendor: 'src/assets/vendor/'
  },
  css: {
    input: 'src/scss/',
    output: 'dist/css/',
    vendor: 'src/scss/**/vendor-main.scss'
  },
  html: {
    input: 'src/html/*',
    output: 'dist/'
  }
};

/***************************
 Gulp Tasks
************************** */

// BrowserSync
function browserSync(done) {
  browsersync.init({
    server: {
      baseDir: paths.output
    },
    port: 3000
  });
  done();
}

// BrowserSync Reload
function browserSyncReload(done) {
  browsersync.reload();
  done();
}

// Clean the whole dist folder
function clean() {
  return del(paths.output);
}

// Copy images
function images() {
  return gulp
    .src(paths.imgs.input)
    .pipe(plumber())
    .pipe(gulp.dest(paths.imgs.output))
}

// Copy fonts
function fonts() {
  return gulp
    .src(paths.fonts.input)
    .pipe(plumber())
    .pipe(gulp.dest(paths.fonts.output))
}

// Copy HTML
function html() {
  return gulp
    .src(paths.html.input)
    .pipe(plumber())
    .pipe(gulp.dest(paths.html.output))
}

// CSS task
function css() {
  return gulp
    .src(path.join(paths.css.input, '**', 'main.scss'))
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass({ 
      outputStyle: "expanded",
      sourceComments: true 
    })) // converts SCSS into CSS
    .pipe(pixrem({
      atrules: true
    })) // adds px instead of rems for older browsers
    .pipe(postcss([autoprefixer() ])) // adds vendor prefixes
    .pipe(sourcemaps.write()) // adds sourcemaps with SCSS locations
    .pipe(gulp.dest(paths.css.output)) // creates beautified CSS files
    .pipe(rename({ suffix: ".min" })) // makes a copy of the file with .min suffix
    .pipe(postcss([cssnano() ])) // minifies the .min.css
    .pipe(gulp.dest(paths.css.output))
    //.pipe(browsersync.stream());
}

// Lint js
// function jsLint() {
//   return gulp
//     .src([paths.js.input, "./gulpfile.js"])
//     .pipe(plumber())
//     .pipe(eslint())
//     .pipe(eslint.format())
//     .pipe(eslint.failAfterError())
// }

// Concatenate and minify js
function js() {
  return (
    gulp
      .src([paths.js.input])
      .pipe(plumber())
      .pipe(concat('main.min.js'))
      .pipe(uglify())
      .pipe(gulp.dest(paths.js.output))
      .pipe(browsersync.stream())
  );
}

// Compile and minify vendor CSS & JS files
function vendor() {

  var vendorCSS = gulp.src(paths.css.vendor)
      .pipe(plumber())
      .pipe(sass({ 
        outputStyle: "expanded",
        sourceComments: false 
      })) // converts SCSS into CSS
      .pipe(pixrem({
        atrules: true
      })) // adds px instead of rems for older browsers
      .pipe(postcss([autoprefixer() ])) // adds vendor prefixes
      .pipe(concat('vendor.min.css'))
      .pipe(postcss([cssnano() ]))
      .pipe(gulp.dest(paths.css.output))

  var vendorJS = gulp.src([
    path.join(paths.js.vendor, '**', 'js', 'jquery-3.3.1.js'),
    path.join(paths.js.vendor, '**', 'js', 'popper.js'),
    path.join(paths.js.vendor, '**', 'js', 'bootstrap.js')
    ])
      .pipe(plumber())
      .pipe(concat('vendor.min.js'))
      .pipe(uglify())
      .pipe(gulp.dest(paths.js.output))

  return merge(vendorJS, vendorCSS);
}

/***************************
 Watch files
************************** */

function watchFiles() {
  gulp.watch(paths.imgs.input, images);
  gulp.watch(path.join(paths.css.input, '**', '*'), gulp.series(css, vendor, browserSyncReload));
  gulp.watch(paths.js.input, gulp.series(js, browserSyncReload));
  gulp.watch(paths.html.input, gulp.series(html, browserSyncReload));
}

/***************************
 Define compound tasks
************************** */

const build = gulp.series(clean, gulp.parallel(images, fonts, html, css, js, vendor));
const watch = gulp.parallel(watchFiles, browserSync);
const buildWatch = gulp.series(clean, gulp.parallel(images, fonts, html, css, js, vendor), gulp.parallel(watchFiles, browserSync));

/***************************
 Export tasks
************************** */

exports.clean = clean;
exports.images = images;
exports.fonts = fonts;
exports.css = css;
exports.vendor = vendor;
exports.js = js;
exports.html = html;

exports.build = build;
exports.watch = watch;
exports.default = buildWatch;