# Casera Web App

Contains code for Casera Home Visit Guide Web App. More information about the app can be found on the [App Wiki](https://bitbucket.org/digitalmeaning/casera-web-app/wiki/Home).

## Development set up

While most of the code in this codebase is HTML, CSS and JavaScript, we make use of [Gulp 4](https://gulpjs.com/) to facilitate certain tasks, such as compiling SCSS and building production ready code.

In order to set up the development environment on your local machine, you need to make sure you have Gulp 4 is installed. If you don't have Gulp 4 set up, you'll need to [make sure you have NPM installed](https://nodejs.org/en/download/) and then open up a terminal and run the following command:

```
npm install gulp-cli -g
```

With Gulp installed, you can now load up the dependencies required by this project. In your terminal, go to the root directory in which you've checked out your working copy of the project and NPM Install, like so:

```
cd /path/to/project/root
npm install
```

Now you've got everything you need to run the Gulp tasks in this project. Let's take a look at the individual tasks that you'll need regularly.

- `gulp` - This is the default task. It runs the build process, and starts watching for updates to SCSS, Javascript, and HTML files that you change in the `/src` directory. Once it detects that one of the files in `/src` directory has been changed it recompiles this type of files and refreshes open HTML files in the browser. Built files are saved in `/dist` directory.
- `gulp build` - runs only the build process.
- `gulp css` - This compiles the SCSS into CSS files for use on the site.
- `gulp js` - This compiles the JS files for use on the site.
- `gulp vendor` - This compiles third-party vendor CSS and JS into separate `vendor.min.css` and `vendor.min.js` files.

For more information on Gulp tasks look into `gulpfile.js` in the project root. 

Remember, each of these tasks is run in your terminal from the root directory of the project.
